package hellopackage;

import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        int input;

        System.out.println("enter a integer");
        input = scan.nextInt();

        int doubled = Utilities.doubleMe(input);
        System.out.println(doubled);

    }
}